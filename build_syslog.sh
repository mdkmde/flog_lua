LUA_VERSION=5.3
CC="gcc -std=gnu99"
CC="tcc"
CFLAGS="-shared -fPIC -O2 -pedantic -Wall"
COMMON_LIBS="-I/usr/include/lua${LUA_VERSION} -llua${LUA_VERSION}"
COMMON_LIBS="-I/usr/include/lua${LUA_VERSION} -L/usr/lib/lua${LUA_VERSION} -llua"

# use sd-journal instead of syslog
#CFLAGS="${CFLAGS} -DJOURNAL"
#COMMON_LIBS="${COMMON_LIBS} -lsystemd"

eval ${CC} ${CFLAGS} -o flog_syslog.so ${COMMON_LIBS} flog_syslog.c

