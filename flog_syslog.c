/*
https://chiselapp.com/mdkmde/flog_lua/ - matthias (at) koerpermagie.de

Copyright (c) ISC License

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <lua.h>
#include <lauxlib.h>
#include <errno.h>
#include <syslog.h>
#ifdef JOURNAL
 #include <systemd/sd-journal.h>
#endif

static int l_syslog_write(lua_State *L) {
  const char *identifier = luaL_checkstring(L, 1);
  const int priority = luaL_checkinteger(L, 2);
  const char *message = luaL_checkstring(L, 3);

  switch(priority) {
    case LOG_EMERG:
    case LOG_ALERT:
    case LOG_CRIT:
    case LOG_ERR:
    case LOG_WARNING:
    case LOG_NOTICE:
    case LOG_INFO:
    case LOG_DEBUG:
#ifdef JOURNAL
      sd_journal_send("SYSLOG_IDENTIFIER=%s", identifier, "PRIORITY=%i", priority, "MESSAGE=%s", message, NULL);
#else
      openlog(identifier, 0, 0);
      syslog(priority, "%s", message);
#endif
  }
  return 0;
}

static const struct luaL_Reg flog_syslog[] = {
  {"write", l_syslog_write},
  {NULL, NULL}
};

int luaopen_flog_syslog(lua_State *L) {
  luaL_newlib(L, flog_syslog);
  lua_pushstring(L, "emerg");
  lua_pushinteger(L, LOG_EMERG);
  lua_rawset(L, -3);
  lua_pushstring(L, "alert");
  lua_pushinteger(L, LOG_ALERT);
  lua_rawset(L, -3);
  lua_pushstring(L, "crit");
  lua_pushinteger(L, LOG_CRIT);
  lua_rawset(L, -3);
  lua_pushstring(L, "err");
  lua_pushinteger(L, LOG_ERR);
  lua_rawset(L, -3);
  lua_pushstring(L, "warning");
  lua_pushinteger(L, LOG_WARNING);
  lua_rawset(L, -3);
  lua_pushstring(L, "notice");
  lua_pushinteger(L, LOG_NOTICE);
  lua_rawset(L, -3);
  lua_pushstring(L, "info");
  lua_pushinteger(L, LOG_INFO);
  lua_rawset(L, -3);
  lua_pushstring(L, "debug");
  lua_pushinteger(L, LOG_DEBUG);
  lua_rawset(L, -3);
  return 1;
}

