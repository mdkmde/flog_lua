--[[
https://chiselapp.com/mdkmde/flog_lua/ - matthias (at) koerpermagie.de

Copyright (c) ISC License

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
]]

local _M = {}
local pairs, pcall, setmetatable, tonumber = pairs, pcall, setmetatable, tonumber
local io = {open = io.open, popen = io.popen, stderr = io.stderr, stdout = io.stdout}
local os = {date = os.date, getenv = os.getenv, time = os.time}
local string = {find = string.find, format = string.format, gsub = string.gsub, sub = string.sub}
_ENV = nil

local DEFAULT_LOG_FORMAT_DATE = "!%Y/%m/%d-%H:%M:%S"
local DEFAULT_LOG_FORMAT_MESSAGE = "=d= =l= =m=\n"
local CONTROL_CHAR_PATTERN = "[\r\n\"]"
local CONTROL_CHAR_REPLACE = {
  ["\r"] = "<CR>",
  ["\n"] = "<LF>",
  ["\""] = "\\\""
}

local formatMessage = function(is_message, ...)
-- returns formated log message
  local b_success, s_message = pcall(string.format, is_message, ...)
  if not(b_success) then
    s_message = string.format("%s: %s", s_message, is_message)
  end
  return (string.gsub(s_message, CONTROL_CHAR_PATTERN, CONTROL_CHAR_REPLACE))
end

--[[
file modes - writer default: open, write, close
  n: syslog.[emerg|alert|crit|err|warning|notice|info|debug]
  | file -> pipe (|logger)
  > new file (default append)
  - only once open, continuous write
  = only once open, continuous write, flush
  (d( date format )d)
  (m( message format )m) -> =d= date, =l= level, =m= message
]]
local checkLogFileMode
checkLogFileMode = function(is_log_file_name, i_syslogWrite, it_log_file_mode)
-- returns log file name and file mode table
  local t_log_file_mode = it_log_file_mode or {}
  if i_syslogWrite and string.find(is_log_file_name, "^%d:") then
    return is_log_file_name, { b_syslog = true }
  elseif not(string.find(is_log_file_name, "^[-=|>%(]")) then
    return is_log_file_name, t_log_file_mode
  end
  local s_mode_char = string.sub(is_log_file_name, 1, 1)
  local s_log_file_name = s_mode_char == "(" and
    string.gsub(is_log_file_name, "^%(([dm])%((.*)%)%1%)(.*)", function(is_format_type, is_format, is_log_file_name)
      local t_format_type = {d = "s_log_format_date", m = "s_log_format_message"}
      t_log_file_mode[t_format_type[is_format_type]] = is_format
      return is_log_file_name
    end) or string.sub(is_log_file_name, 2)
  if s_mode_char == ">" then
    t_log_file_mode.b_write = true
  elseif s_mode_char == "|" then
    t_log_file_mode.b_pipe = true
  elseif s_mode_char == "-" then
    t_log_file_mode.b_continue = true
  elseif s_mode_char == "=" then
    t_log_file_mode.b_continue = true
    t_log_file_mode.b_flush = true
  end
  return checkLogFileMode(s_log_file_name, i_syslogWrite, t_log_file_mode)
end

local getFileWriter = function(is_log_file_name, it_log_file_mode, it_log_fd)
-- returns writer function and fd
  local fw_out
  if it_log_fd[is_log_file_name] and it_log_file_mode.b_continue then
    fw_out = it_log_fd[is_log_file_name]
  elseif it_log_file_mode.b_pipe then
    fw_out = io.popen(is_log_file_name, "w")
  else
    fw_out = io[is_log_file_name] or io.open(is_log_file_name, it_log_file_mode.b_write and "w" or "a")
  end
  if not(fw_out) then
    return
  end
  if it_log_file_mode.b_continue then
    if it_log_file_mode.b_flush then
     return function(ifw_out, is_message)
        ifw_out:write(is_message):flush()
      end, fw_out
    end
    return function(ifw_out, is_message)
      ifw_out:write(is_message)
    end, fw_out
  end
  fw_out:close()
  if it_log_file_mode.b_pipe then
    return function(is_log_file_name, is_message)
      fw_out = io.popen(is_log_file_name, "w")
      if fw_out then
        fw_out:write(is_message):close()
      end
    end
  end
  return function(is_log_file_name, is_message)
    fw_out = io[is_log_file_name] or io.open(is_log_file_name, "a")
    if fw_out then
      fw_out:write(is_message):close()
    end
  end
end

local getSyslogWriteFunction = function(is_log_file_name, i_syslogWrite)
-- returns log_writer function
  local s_log_identifier = 2 < #is_log_file_name and string.sub(is_log_file_name, 3) or "flog_lua"
  local n_log_priority = tonumber(string.sub(is_log_file_name, 1, 1))
  return function(...)
    i_syslogWrite(s_log_identifier, n_log_priority, formatMessage(...))
  end
end

local getWriteFunction = function(is_log_level, is_log_file_name, it_log_file_mode, is_log_format_message, is_log_format_date, it_log_fd)
-- returns log_writer function and fd
  local write, fw_out = getFileWriter(is_log_file_name, it_log_file_mode, it_log_fd)
  local s_log_format_message = string.gsub(it_log_file_mode.s_log_format_message or is_log_format_message, "=l=", is_log_level)
  local s_log_format_date = string.gsub(it_log_file_mode.s_log_format_date or is_log_format_date, CONTROL_CHAR_PATTERN, CONTROL_CHAR_REPLACE)
  local x_log_dest = it_log_file_mode.b_continue and fw_out or is_log_file_name
  return function(...)
    write(x_log_dest, (string.gsub(s_log_format_message, "=([dm])=", {
      ["d"] = os.date((string.gsub(s_log_format_date, "%%s", os.time()))),
      ["m"] = formatMessage(...)
    })))
  end, fw_out
end

local createLogWriter = function(it_setup, is_log_format_message, is_log_format_date)
-- returns tables log_writer and fd
  local t_log_writer, t_log_fd = {}, {}
  local t_ignore_format_string = {["%e"] = true, ["%d"] = true, ["%m"] = true, ["%s"] = true}
  local syslogWrite = it_setup["%s"] and it_setup["%s"].write
  for s_log_level, s_log_file_data in pairs(it_setup) do
    if not(t_ignore_format_string[s_log_level]) then
      local s_log_file_name, t_log_file_mode = checkLogFileMode(s_log_file_data, syslogWrite)
      local writer, fw_out = t_log_file_mode.b_syslog and
        getSyslogWriteFunction(s_log_file_name, syslogWrite) or
        getWriteFunction(s_log_level, s_log_file_name, t_log_file_mode, is_log_format_message, is_log_format_date, t_log_fd)
      if writer then
        t_log_writer[s_log_level] = writer
        if fw_out then
          t_log_fd[s_log_file_name] = fw_out
        end
      end
    end
  end
  return t_log_writer, t_log_fd
end

local setupLogWriter = function(it_setup)
-- returns log_writer (table with writer functions) and log_fd for gc
  local s_log_format_message = it_setup["%m"] or DEFAULT_LOG_FORMAT_MESSAGE
  if it_setup["%d"] == "" then
    s_log_format_message = string.gsub(s_log_format_message, "=d=", "")
  end
  return createLogWriter(it_setup, s_log_format_message, it_setup["%d"] or DEFAULT_LOG_FORMAT_DATE)
end

local createLogWriterMetatable = function(it_log_fd)
-- returns log_writer_metatable gc and undefined loglevel default function
  local undefined_loglevel = function() end
  return {
    __gc = function()
      for _, fw_out in pairs(it_log_fd) do
        fw_out:flush()
        fw_out:close()
      end
    end,
    __index = function(iot_self, i_key)
      iot_self[i_key] = undefined_loglevel
      return undefined_loglevel
    end
  }
end

local init = function(it_setup)
-- returns log_writer (table with writer functions)
  local t_setup = it_setup or {}
  if it_setup["%e"] then
    local f_env = io.popen("env")
    local s_env = f_env:read("a") -- "*a" for Lua 5.2
    f_env:close()
    local getenv = function(is_env)
      local t_replace_control_char = {["\\n"] = "\n", ["\\r"] = "\r", ["\t"] = "\t"}
      return string.gsub(os.getenv(is_env), "\\[nrt]", t_replace_control_char)
    end
    string.gsub(s_env, "%s(LOG_[^=]+)=", function(is_env_log_var)
      if is_env_log_var == "LOG_FORMAT_DATE" then
        t_setup["%d"] = getenv("LOG_FORMAT_DATE")
      elseif is_env_log_var == "LOG_FORMAT_MESSAGE" then
        t_setup["%m"] = getenv("LOG_FORMAT_MESSAGE")
      else
        t_setup[string.gsub(is_env_log_var, "LOG_", "")] = getenv(is_env_log_var)
      end
    end)
  end
  local t_log_writer, t_log_fd = setupLogWriter(t_setup)
  local t_log_writer_metatable = createLogWriterMetatable(t_log_fd)
  setmetatable(t_log_writer, t_log_writer_metatable)
  return t_log_writer
end

return {
  init = init, --[[ (log data table: {
      err=string.format("%d:identifier", syslog.err),
      file="/var/log/file.log",
      pipe="|cat",
      stdout="(m(=d= =m=)m)(d(%s)d)=stdout",
      -- optional fields for env and global format setup:
      ["%e"] = false,
      ["%d"] = "!%Y/%m/%d-%H:%M:%S",
      ["%m"] = "=d= =l= =m=\n",
      ["%s"] = syslog,
    })
    returns log_writer (table with writer functions) ]]
}

